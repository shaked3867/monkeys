from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login
from web.models import *


def loginP(request):
    if request.method == 'POST':
        #if the user try to register section
        if request.POST.get('fname') and request.POST.get('username') and request.POST.get('password') and request.POST.get('conpass'):

            try:
                fullname = str(request.POST.get('fname')).split()
                firstname = fullname[0]
                lastname = fullname[1]
            except Exception as ex:
                messages.error(request, 'full name please')
                return render(request, 'loginP.html')

            usrnm = str(request.POST.get('username'))
            password = str(request.POST.get('password'))
            conpass = str(request.POST.get('conpass'))

            try:
                u = User.objects.get(username=usrnm)
                messages.error(request, 'username already exists')
                return render(request, 'loginP.html')
            except User.DoesNotExist:
                pass

            if password == conpass:
                pass
            else:
                messages.error(request, 'passwords does not match')
                return render(request, 'loginP.html')

            nuser = User.objects.create_user(usrnm, None, password)
            nuser.last_name = lastname
            nuser.first_name = firstname
            nuser.save()
            nuser = authenticate(request, username=usrnm, password=password)
            login(request, nuser)
            person = {'firstname': firstname.upper(), 'lastname': lastname.upper(), 'username': usrnm}
            context = {'person': person}
            return render(request, 'index.html', context)
        # if the user try to log in section
        elif request.POST.get('loginusrn') and request.POST.get('loginpass'):
            usrnm = str(request.POST.get('loginusrn'))
            password = str(request.POST.get('loginpass'))
            usr = authenticate(request, username=usrnm, password=password)
            if usr is not None:
                login(request, usr)
                username = str(usr.get_username())
                fullname = str(usr.get_full_name()).split()
                firstname = fullname[0]
                lastname = fullname[1]
                person = {'firstname': firstname.upper(), 'lastname': lastname.upper(), 'username': username}
                context = {'person': person}
                return render(request, 'index.html', context)
            else:
                messages.success(request, 'username or password are incorrect')
                return render(request, 'loginP.html')
        else:
            return render(request, 'loginP.html')
    else:
        return render(request, 'loginP.html')


