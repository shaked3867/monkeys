document.write(`

    <!--=====================================================
\t\t\t\tMenu Button
\t\t\t=====================================================-->
\t\t\t<a href='#' class='menu-btn' >
\t\t\t\t<span class='lines' >
\t\t\t\t\t<span class='l1' ></span>
\t\t\t\t\t<span class='l2' ></span>
\t\t\t\t\t<span class='l3' ></span>
\t\t\t\t</span>
\t\t\t</a>


\t\t\t<!--=====================================================
\t\t\t\tMenu
\t\t\t=====================================================-->
\t\t\t<div class='menu' >
\t\t\t\t<div class='inner' >
\t\t\t\t\t<ul class='menu-items' >

\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href='#' class='section-toggle' data-section='homepage' >
\t\t\t\t\t\t\t\tHome
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href='#' class='section-toggle' data-section='about' >
\t\t\t\t\t\t\t\tAbout
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href='#' class='section-toggle' data-section='resume' >
\t\t\t\t\t\t\t\tResume
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href='#' class='section-toggle' data-section='portfolio' >
\t\t\t\t\t\t\t\tPortfolio
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>


\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href='#' class='section-toggle' data-section='contact' >
\t\t\t\t\t\t\t\tContact
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href="{% url 'index' %}" class='section-toggle' >
\t\t\t\t\t\t\t\tLogOut
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>


\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class='animation-block' ></div>

`);